export function querySelect(params) {
  return new Promise((resolve, reject) => {
    const query = wx.createSelectorQuery();
    query.select(params).boundingClientRect();
    query.exec((res) => {
      resolve(res)
    });
  })
}
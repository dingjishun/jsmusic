// pages/detail-song/detail-song.js
import rankingStore from '../../store/rankingStore'
import recommendStore from '../../store/recommendStore'
import {
  getPlayListDetail
} from '../../service/music'
Page({
  data: {
    type: '',
    key: '',
    songInfos: {},
    id: ''
  },
  onLoad(options) {
    const type = options.type;
    // this.data.type = type;
    this.setData({type})
    const key = options.key;
    this.data.key = key;
    if (type === 'ranking') {
      rankingStore.onState(key, this.handleRanking)
    }
    if (type === 'recommend') {
      recommendStore.onState('recommendSongInfo', this.handleRanking)
    }
    if (type === 'menu') {
      const id = options.id
      this.data.id = id;
      this.fetchMenuSongInfo()
    }
  },
  async fetchMenuSongInfo() {
    const res = await getPlayListDetail(this.data.id)
    this.setData({songInfos:res.playlist})
  },
  handleRanking(value) {
    this.setData({
      songInfos: value
    });
    if (value.name === '热歌榜') {
      wx.setNavigationBarTitle({
        title: '推荐榜单排名',
      })
    } else {
      wx.setNavigationBarTitle({
        title: value.name,
      })
    }

  },
  onUnload() {
    if (this.data.type === 'ranking') {
      rankingStore.offState(this.data.key, this.handleRanking)
    }
    if (this.data.type === 'ranking') {
      recommendStore.offState("recommendSongInfo", this.handleRanking)
    }
  }
})
import {
  getTopMv
} from "../../service/video";

// pages/main-video/main-video.js
Page({
  data: {
    videoList: [],
    offset: 0,
    hasMore: true
  },
  onLoad() {
    // 发送网络请求
    this.fetchTopMv();
  },
  // 发送网络请求函数
  async fetchTopMv() {
    let res = await getTopMv(this.data.offset)
    let newVideoList = [...this.data.videoList, ...res.data]
    this.setData({
      videoList: newVideoList
    })
    this.data.offset = this.data.videoList.length
    this.data.hasMore = res.hasMore;


  },
  // 监听上拉和下拉功能
  onReachBottom() {
    if (this.data.hasMore) {
      this.fetchTopMv();
    } else {
      return;
    }
  },
  // 下拉刷新
  onPullDownRefresh() {
    // 1、清空数据
    this.setData({
      videoList: []
    });
    this.data.offset = 0;
    this.data.hasMore = true;

    // 2、重新请求新数据
    this.fetchTopMv()

    // 4、数据请求成功后停止下拉刷新
    wx.stopPullDownRefresh().then(() => {
      // 4、数据请求成功后停止下拉刷新
      wx.stopPullDownRefresh();
    })
  },
  // 事件监听
  onVideoItemTap(event){
    // console.log(event.currentTarget.dataset.item);
    const item = event.currentTarget.dataset.item;
    // 跳转页面并且给页面发送数据
    wx.navigateTo({
      url: `/pages/detail-video/detail-video?id=${item.id}`,
    })
  }
})
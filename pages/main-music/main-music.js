import {
  getMusicBanner,
  getPlayListDetail,
  getSongMenuList
} from "../../service/music"
import {
  querySelect
} from "../../utils/query-select"
import {
  throttle
} from 'underscore'
import recommendStore from '../../store/recommendStore'
import rankingStore from "../../store/rankingStore"

const querySelectThrottle = throttle(querySelect, 2000)

// pages/main-music/main-music.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchValue: "",
    banners: [],
    bannerHeight: 130,
    // 推荐歌曲数据
    recommendSongs: [],
    // 热门歌单数据
    songMenuList: [],
    // 推荐歌单
    recMenuList: [],
    // 巅峰榜数据
    rankingInfos: {}
  },
  onLoad() {
    this.getBanner(0);
    // this.fetchPlayListDetail();
    recommendStore.onState("recommendSongs", this.handleRecommendSongs)
    recommendStore.dispatch("fetchRecommendSongsAction");
    rankingStore.onState("newRanking",this.handleNewRanKing);
    rankingStore.onState("originRanking",this.handleOriginRanking);
    rankingStore.onState("upRanking",this.handleUpRanking);
    rankingStore.dispatch("fetchRankingDataAction")
    this.fetchSongMenuList();
    this.fetchSongMenuList2();
  },
  async getBanner(params) {
    const res = await getMusicBanner(params)
    this.setData({
      banners: res.banners
    })
  },
  async fetchSongMenuList() {
    const res = await getSongMenuList();
    this.setData({
      songMenuList: res.playlists
    })
  },
  async fetchSongMenuList2() {
    const res = await getSongMenuList("华语");
    this.setData({
      recMenuList: res.playlists
    })
  },
  // 监听搜索框
  onSearchClick() {
    wx.navigateTo({
      url: '/pages/detail-search/detail-search',
    })
  },
  async onBannerImage(event) {
    // 获取image组件的高度
    const res = await querySelectThrottle(".Bimage");
    this.setData({
      bannerHeight: res[0].height
    })
  },
  onRecommendMoreClick() {
    wx.navigateTo({
      url: '/pages/detail-song/detail-song?type=recommend',
    })
  },
  // async fetchPlayListDetail(){
  //   const res = await getPlayListDetail(3778678);
  //   this.setData({recommendSongs:res.playlist.tracks.slice(0,6)})
  // }
  onMenuMoreClick() {
    wx.navigateTo({
      url: '/pages/detail-menu/detail-menu',
    })
  },
  // 从Store中获取数据
  handleRecommendSongs(value) {
    this.setData({
      recommendSongs: value.slice(0, 6)
    })
  },
  handleNewRanKing(value){
    const newRankingInfos = {...this.data.rankingInfos,newRanking:value}
    this.setData({rankingInfos:newRankingInfos})

  },
  handleOriginRanking(value){
    const newRankingInfos = {...this.data.rankingInfos,originRanking:value}
    this.setData({rankingInfos:newRankingInfos})
  },
  handleUpRanking(value){
    const newRankingInfos = {...this.data.rankingInfos,upRanking:value}
    this.setData({rankingInfos:newRankingInfos})
  },
  onunload(){
    recommendStore.offState("recommendSongs",this.handleRecommendSongs);
  }
})
// pages/detail-video/detail-video.js
import {
  getMvUrl,
  getMVInfo,
  getMVRelate
} from '../../service/video'
Page({
  data: {
    id: 0,
    mvUrl: null,
    mvInfo:{},
    MVRelate:[],
    danmuList: [{
        text: '我是要成为海贼王的男人',
        color: '#fff',
        time: 6
      },
      {
        text: '木叶飞舞之处，火亦生生不息。',
        color: '#fff',
        time: 10
      },
      {
        text: '三十年河东，三十年河西，莫欺少年穷。',
        color: '#fff',
        time: 16
      },
      {
        text: '我将以高达形态出击',
        color: '#fff',
        time: 14
      },
      {
        text: '纵使我身形俱灭，也定将恶鬼斩杀',
        color: '#fff',
        time: 16
      },
      {
        text: '错的不是我 错的是这个世界。',
        color: '#fff',
        time: 7
      },
    ],
  },
  onLoad(options) {
    // 获取视频id
    const id = options.id;
    this.setData({
      id: id
    })
    this.fetchMVurl(id);
    this.fetchMVInfo();
    this.fetchMVRelate();
  },
  async fetchMVurl(id) {
    // 2、请求视频数据
    const res = await getMvUrl(id)
    const url = res.data.url;
    this.setData({
      mvUrl: url
    })
  },
  async fetchMVInfo(){
    const res = await getMVInfo(this.data.id);
    this.setData({mvInfo:res.data})
  },
  async fetchMVRelate(){
    const res =await getMVRelate(this.data.id);
    this.setData({MVRelate:res.data})
  }
})
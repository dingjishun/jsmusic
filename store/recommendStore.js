import {HYEventStore} from 'hy-event-store'
import {getPlayListDetail} from '../service/music'

const recommendStore = new HYEventStore({
  state:{
    recommendSongs:[],
    recommendSongInfo:{},
  },
  actions:{
    fetchRecommendSongsAction(ctx){
      getPlayListDetail(3778678).then(res=>{
        ctx.recommendSongs = res.playlist.tracks
        ctx.recommendSongInfo = res.playlist
      })
    }
  }
}); 

export default recommendStore